﻿# openharmony_softbus_tool
## 简介
softbus_tool测试工具是将软总线interfaces目录下的一些常用接口集中起来，供设备间搭建一些场景时使用（绑定，br组网，ble组网，
指定p2p链路传输）。
一般使用流程：1.使用编译命令生成可执行文件; 2.将生成的可执行文件推送到设备；3.在hdc shell界面执行，会显示
Softbus Tool Command List命令列表，选择接口前面的序号即可执行相应的接口，有些接口在执行时，需要用户输入参数来实现不同的场景，
这些在下文演示部分做了示例。
### 1. Softbus Tool Command List
Softbus Tool Command List是Softbus_Tool测试工具提供的所有指令列表，为了避免内容过长，在后文演示部分只列出了要执行的某一条指令。
```text
******Softbus Tool Command List******
*     00 - PublishService           *
*     01 - UnPublishService         *
*     02 - StartDiscovery           *
*     03 - StopDiscovery            *
*     04 - JoinLNN                  *
*     05 - LeaveLNN                 *
*     06 - GetLocalDeviceInfo       *
*     07 - GetOnlineDeviceInfo      *
*     08 - GetBtMac                 *
*     09 - CreateSessionServer      *
*     10 - RemoveSessionServer      *
*     11 - OpenSession              *
*     12 - CloseSession             *
*     13 - SetFileSendListener      *
*     14 - SetFileRecvListener      *
*     15 - SendMessage              *
*     16 - SendBytes                *
*     17 - SendStream               *
*     18 - SendFile                 *
*     19 - Exit                     *
*************************************
```

## 绑定
设备间在组网前，需要绑定，绑定成功才可以进行后续组网，传输功能。
目前有两种绑定的方式可供参考。
### 1. 在设备上进行绑定

   1. 启动A，B两端设备，打开设置->WLAN，连接同一WiFi；
   2. A端打开计算器，点击右上角“向上→”图标，B端设备上会弹出6位数字的验证图片；
   3. 在A端输入6位验证码；
   4. 绑定成功；

### 2. 执行工具进行绑定
   1. 启动A，B两端设备，打开设置->WLAN，连接同一WiFi；
   2. 执行bind_tool工具，可参照[openharmony_softbus_tool/Downloads/bind_tool/说明.txt]。

## 打开Selinux接口管控
    `ps -ef | grep "accesstoken_ser" | awk '{print $2}' | xargs kill`

## 组网

### 1. WiFi组网

   1. 确认A，B两端设备均已打开wifi开关，且已连接同一WiFi网络。
   2. A端执行 0 - PublishService，发布服务。
   3. B端执行 2 - StartDiscovery，订阅服务。
   4. B端执行 4 - JoinLNN，组网。

> *演示：*

    1. 确认A，B两端设备均已打开wifi开关，且已连接同一网络
    2. A端执行 0 - PublishService，发布服务
```text
******Softbus Tool Command List******
*               ...                 *
*     00 - PublishService           *
*               ...                 *
*************************************
==>Please input cmd index:0    /* 输入0，回车 */

Execute: PublishService
==>Please input publish id:1    /* 输入预设值，回车 */
==>Please input publish medium(0 - AUTO, 1 - BLE, 2 - COAP):2    /* 输入2选择COAP，回车 */
>>>OnPublishSuccess publishId = 1.    /* 发布成功 */
```
    3. B端执行 2 - StartDiscovery，订阅服务
```text
******Softbus Tool Command List******
*               ...                 *
*     02 - StartDiscovery           *
*               ...                 *
*************************************
==>Please input cmd index:2     /* 输入2，回车 */

Execute: StartDiscovery
==>Please input subscribe id:1      /* 输入A端发布服务时预设的publish id值，回车 */
==>Please input subscribe medium(0 - AUTO, 1 - BLE, 2 - COAP):2    /* 输入2选择COAP，回车 */
>>>OnDiscoverySuccess subscribeId = 1.    /* 订阅成功 */
==>Found Devices shown below, please input ENTER key to break.
>>>OnDeviceFound: IP ADDR:Port - 192.168.43.4:40563  |  DevId - 69E9948F4FFDB40C86C3B4A6273
F0BD400D80E55E72629726C27FFE3D3398B04  /* 发现的设备 */
```
    4. B端执行 4 - JoinLNN，组网
```text
******Softbus Tool Command List******
*               ...                 *
*     04 - JoinLNN                  *
*               ...                 *
*************************************
==>Please input cmd index:4    /* 输入4，回车 */

Execute: JoinLNN
==>Please input connection type(0 - WiFi, 1 - BR, 2 - BLE, 3 - ETH):1    /* 输入0选择WiFi，回车 */
==>Please input ip:192.168.43.4        /* 输入上一步发现的设备的IP ADDR值，回车 */
==>Please input port:40563        /* 输入上一步发现的设备的Port值，回车 */
>>>OnJoinLNNResult networkId = d713af167c6342c83f4335d36b35938beb5f4ac6524f1f302bcf211f
086d80c9, retCode = 0.    /* 组网成功 */
```
### 2. WiFi退网

   1. A端（或B端）执行 6 - GetLocalDeviceInfo，查看本端NetworkId值。
   2. B端（或A端）执行 5 - LeaveLNN，退网。

> *演示：*

    1. A端（或B端）执行 6 - GetLocalDeviceInfo，查看本端NetworkId值
```text
******Softbus Tool Command List******
*               ...                 *
*     06 - GetLocalDeviceInfo       *
*               ...                 *
*************************************
==>Please input cmd index:6    /* 输入6，回车 */

Execute: GetLocalDeviceInfo
>>>DeviceName: OpenHarmony 3.1 Release
>>>DeviceNetworkId: d713af167c6342c83f4335d36b35938beb5f4ac6524f1f302bcf211f086d80c9    /* 本端NetworkId值 */
>>>Udid: 69E9948F4FFDB40C86C3B4A6273F0BD400D80E55E72629726C27FFE3D3398B04
>>>Uuid: 5e201aef76d538179e02970c099e302aa894dc3371a5ce6736493223e54c44e1
>>>ShortDevId: 6529d122
```
    2. B端（或A端）执行 5 - LeaveLNN，退网。
```text
******Softbus Tool Command List******
*               ...                 *
*     05 - LeaveLNN                 *
*               ...                 *
*************************************
==>Please input cmd index:5    /* 输入5，回车 */

Execute: LeaveLNN
==>Please input network Id:d713af167c6342c83f4335d36b35938beb5f4ac6524f1f3
02bcf211f086d80c9     /* 输入A端查看的NetworkId值，回车 */
>>>OnLeaveLNNDone networkId = d713af167c6342c83f4335d36b35938beb5f4ac6524f1f3
02bcf211f086d80c9, retCode = 0.    /* 退网成功 */
```

### 3. Ble组网

   1. 确认A，B两端设备均已打开蓝牙开关。
   2. A端执行 0 - PublishService，发布服务。
   3. B端执行 2 - StartDiscovery，订阅服务。
   4. B端执行 4 - JoinLNN，组网。 

> *演示：*

    1. 确认A，B两端设备均已打开蓝牙开关
    2. A端执行 0 - PublishService，发布服务
```text
******Softbus Tool Command List******
*               ...                 *
*     00 - PublishService           *
*               ...                 *
*************************************
==>Please input cmd index:0    /* 输入0，回车 */
    
Execute: PublishService
==>Please input publish id:1    /* 输入预设值，回车 */
==>Please input publish medium(0 - AUTO, 1 - BLE, 2 - COAP):1    /* 输入1选择BLE，回车 */
>>>OnPublishSuccess publishId = 1.    /* 发布成功 */
```
    3. B端执行 2 - StartDiscovery，订阅服务
```text
******Softbus Tool Command List******
*               ...                 *
*     02 - StartDiscovery           *
*               ...                 *
*************************************
==>Please input cmd index:2     /* 输入2，回车 */

Execute: StartDiscovery
==>Please input subscribe id:1      /* 输入A端发布服务时预设的publish id值，回车 */
==>Please input subscribe medium(0 - AUTO, 1 - BLE, 2 - COAP):1    /* 输入1选择BLE，回车 */
>>>OnDiscoverySuccess subscribeId = 1.    /* 订阅成功 */
==>Found Devices shown below, please input ENTER key to break.
>>>OnDeviceFound: DevId - c0c78afa  |  BLE MAC - 67:54:4c:e9:c1:03  /* 发现的设备 */
```
    4. B端执行 4 - JoinLNN，组网
```text
******Softbus Tool Command List******
*               ...                 *
*     04 - JoinLNN                  *
*               ...                 *
*************************************
==>Please input cmd index:4    /* 输入4，回车 */
    
Execute: JoinLNN
==>Please input connection type(0 - WiFi, 1 - BR, 2 - BLE, 3 - ETH):2    /* 输入2选择BLE，回车 */
==>Please input BLE mac:67:54:4c:e9:c1:03    /* 输入上一步发现的设备的BLE MAC值，回车 */
>>>OnJoinLNNResult networkId = 7e435e594d0d7f6daafa385d8bea8999ec7c5f954f34dfd739ec2
5c26f97c998, retCode = 0.    /* 组网成功 */
```
### 4. Ble退网

   1. A端（或B端）执行 6 - GetLocalDeviceInfo，查看本端NetworkId值。
   2. B端（或A端）执行 5 - LeaveLNN，退网。

> *演示：*

    1. A端（或B端）执行 6 - GetLocalDeviceInfo，查看本端NetworkId值
```text
******Softbus Tool Command List******
*               ...                 *
*     06 - GetLocalDeviceInfo       *
*               ...                 *
*************************************
==>Please input cmd index:6    /* 输入6，回车 */
    
Execute: GetLocalDeviceInfo
>>>DeviceName: OpenHarmony 3.1 Release
>>>DeviceNetworkId: 67635a91eab855c757abdd9907c40e793420455e5ff42452c5dfccd7ea0f63a5    /* 本端NetworkId值 */
>>>Udid: 531A287C6FD29E82D943B4CB166EF433330A175ECABCE71578CE45278C3A3077
>>>Uuid: 69af1b8b62fc80ec3d85eeb7a4dde762be89cfe9265b167a0557cf783f94c8b6
>>>ShortDevId: c0c78afa
```
    2. B端（或A端）执行 5 - LeaveLNN，退网。
```text
******Softbus Tool Command List******
*               ...                 *
*     05 - LeaveLNN                 *
*               ...                 *
*************************************
==>Please input cmd index:5    /* 输入5，回车 */
    
Execute: LeaveLNN
==>Please input network Id:67635a91eab855c757abdd9907c40e793420455
e5ff42452c5dfccd7ea0f63a5    /* 输入A端查看的NetworkId值，回车 */
>>>OnLeaveLNNDone networkId = 67635a91eab855c757abdd9907c40e793420455
e5ff42452c5dfccd7ea0f63a5, retCode = 0.    /* 退网成功 */
```
### 5. Br组网

   1. 确认A，B两端设备均已打开蓝牙开关。
   2. A端执行 8 - GetBtMac，查看Bt mac。
   3. B端执行 4 - JoinLNN，组网。
    
> *演示：*

    1. 确认A，B两端设备均已打开蓝牙开关
    2. A端执行 8 - GetBtMac，查看Bt mac
```text
******Softbus Tool Command List******
*               ...                 *
*     08 - GetBtMac                 *
*               ...                 *
*************************************
==>Please input cmd index:8    /* 输入8，回车 */
    
Execute: GetBtMac
BT MAC - [08:fb:ea:1b:3d:15]
```  
    3. B端执行 4 - JoinLNN，组网
```text
******Softbus Tool Command List******
*               ...                 *
*     04 - JoinLNN                  *
*               ...                 *
*************************************
==>Please input cmd index:4    /* 输入4，回车 */
    
Execute: JoinLNN
==>Please input connection type(0 - WiFi, 1 - BR, 2 - BLE, 3 - ETH):1    /* 输入1选择BR，回车 */
==>Please input BT mac:08:fb:ea:1b:a4:3f    /* 输入上一步A端查看的BT mac值，回车 */
>>>OnJoinLNNResult networkId = 669dbaff972895f48f0524f0155f3956dcbec3dd27d6e9feee9ccaf
938cbf5c2, retCode = 0.    /* 组网成功 */
```
### 6. Br退网

   1. A端（或B端）执行 6 - GetLocalDeviceInfo，查看本端networkId。
   2. B端（或A端）执行 5 - LeaveLNN，退网。

> *演示：*

    1. A端（或B端）执行 6 - GetLocalDeviceInfo，查看本端networkId
```text
******Softbus Tool Command List******
*               ...                 *
*     06 - GetLocalDeviceInfo       *
*               ...                 *
*************************************
==>Please input cmd index:6    /* 输入6，回车 */
    
Execute: GetLocalDeviceInfo
>>>DeviceName: OpenHarmony
>>>DeviceNetworkId: c91a5018ade2627725da5915016bc2e182fe0d053d0cfc9aba08076953b010b2
>>>Udid: 531A287C6FD29E82D943B4CB166EF433330A175ECABCE71578CE45278C3A3077
>>>Uuid: 69af1b8b62fc80ec3d85eeb7a4dde762be89cfe9265b167a0557cf783f94c8b6
>>>ShortDevId: c0c78afa
```
    2. B端（或A端）执行 5 - LeaveLNN，退网
```text
******Softbus Tool Command List******
*               ...                 *
*     05 - LeaveLNN                 *
*               ...                 *
*************************************
==>Please input cmd index:5    /* 输入5，回车 */
    
Execute: LeaveLNN
==>Please input network Id:c91a5018ade2627725da5915016bc2e182fe0d053d0cfc9
aba08076953b010b2    /* 输入上一步查看的A端的networkId值，回车 */
>>>OnLeaveLNNDone networkId = c91a5018ade2627725da5915016bc2e182fe0d053d0cfc9aba08
076953b010b2, retCode = 0.    /* 退网成功 */
```

## 传输
   开始传输前，设备间需要组网成功；例如，Ble组网、Br组网、WiFi组网。
### 1. Bytes传输指定p2p链路

   1. A端执行 9 - CreateSessionServer，创建会话服务。
   2. B端执行 9 - CreateSessionServer，创建会话服务。
   3. B端执行 6 - GetLocalDeviceInfo，查看本端NetworkId值。
   4. A端（或B端）执行 11 - OpenSession，打开会话。
   5. A端（或B端）执行 16 - SendBytes，发送Bytes。
   6. A端（或B端）执行 12 - CloseSession，关闭会话。
   7. A端执行 10 - RemoveSessionServer，删除会话服务。
   8. B端执行 10 - RemoveSessionServer，删除会话服务。

> *演示：*

    1. A端执行CreateSessionServer，创建会话服务
```text
******Softbus Tool Command List******
*               ...                 *
*     09 - CreateSessionServer      *
*               ...                 *
*************************************
==>Please input cmd index:9    /* 输入9，回车 */
    
Execute: CreateSessionServer
CreateSessionServer ok.
```        
    2. B端执行CreateSessionServer，创建会话服务
```text    
******Softbus Tool Command List******
*               ...                 *
*     09 - CreateSessionServer      *
*               ...                 *
*************************************
==>Please input cmd index:9    /* 输入9，回车 */
    
Execute: CreateSessionServer
CreateSessionServer ok.
```
    3. B端执行 6 - GetLocalDeviceInfo，查看本端NetworkId值
```text    
******Softbus Tool Command List******
*               ...                 *
*     06 - GetLocalDeviceInfo       *
*               ...                 *
*************************************
==>Please input cmd index:6    /* 输入6，回车 */
    
Execute: GetLocalDeviceInfo
>>>DeviceName: OpenHarmony
>>>DeviceNetworkId: 218b78376cdaec37a3bc75276565bc579d023a41a98754579b605d76aa53733b
>>>Udid: 531A287C6FD29E82D943B4CB166EF433330A175ECABCE71578CE45278C3A3077
>>>Uuid: 69af1b8b62fc80ec3d85eeb7a4dde762be89cfe9265b167a0557cf783f94c8b6
>>>ShortDevId: c0c78afa
```
    4. A端（或B端）执行 11 - OpenSession，打开会话
```text
******Softbus Tool Command List******
*               ...                 *
*     11 - OpenSession              *
*               ...                 *
*************************************
==>Please input cmd index:11    /* 输入11，回车 */
    
Execute: OpenSession
==>Please input session type(1 - TYPE_MESSAGE, 2 - TYPE_BYTES, 3 - TYPE_FILE, 4 -
TYPE_STREAM):2    /* 输入2选择Bytes，回车 */
==>Please input linkType num(0 - AUTO or [1, 4]):1    /* 输入数值来指定支持几种链路方式，输入0为默认，
输入1~4为指定1~4种链路方式，回车 */
Please input linkType[0]==>(1 - WLAN 5G, 2 - WLAN 2G, 3 - WIFI P2P, 4 - BR):3    /* 输入3选择
P2P链路方式，回车 */
==>Please input network Id:218b78376cdaec37a3bc75276565bc579d023a41a98754579b605
d76aa53733b    /* 输入上一步查看的B端的NetworkId值，回车 */
        
>>>OnSessionOpened session id = 1, result = 0.
OpenSession succ, sessionId = 1
      
B端确认：
        
******Softbus Tool Command List******
*               ...                 *
*************************************
==>Please input cmd index:
>>>OnSessionOpened session id = 1, result = 0.
```  
    5. A端（或B端）执行 16 - SendBytes，发送Bytes
```text
******Softbus Tool Command List******
*               ...                 *
*     16 - SendBytes                *
*               ...                 *
*************************************
==>Please input cmd index:16    /* 输入16，回车 */
    
Execute: SendBytes
==>Please input session Id:1    /* 输入上一步打开会话的session id值，回车 */
==>Please input data to SendBytes:TestSendBytes    /* 输入测试数据 */
SendBytes ok
        
B端确认：
>>>OnBytesReceived session id = 1, data = TestSendBytes.
```        
    
    6. A端（或B端）执行 12 - CloseSession，关闭会话
```text
******Softbus Tool Command List******
*               ...                 *
*     12 - CloseSession             *
*               ...                 *
*************************************
==>Please input cmd index:12    /* 输入12，回车 */
    
Execute: CloseSession
==>Please input session Id to CloseSession:1    /* 输入要关闭的会话id，回车 */
        
B端确认：
>>>OnSessionClosed session id = 1.
```
    7. A端执行 10 - RemoveSessionServer，删除会话服务
```text
******Softbus Tool Command List******
*               ...                 *
*     10 - RemoveSessionServer      *
*               ...                 *
*************************************
==>Please input cmd index:10    /* 输入10，回车 */
    
Execute: RemoveSessionServer
```
    8. B端执行 10 - RemoveSessionServer，删除会话服务
```text
******Softbus Tool Command List******
*               ...                 *
*     10 - RemoveSessionServer      *
*               ...                 *
*************************************
==>Please input cmd index:
>>>OnSessionOpened session id = 1, result = 0.    /* 前面步骤的回调信息，已经确认过 */
    
>>>OnBytesReceived session id = 1, data = TestSendBytes.    /* 前面步骤的回调信息，已经确认过 */
    
>>>OnSessionClosed session id = 1.    /* 前面步骤的回调信息，已经确认过 */
10    /* 输入10，回车 */
    
Execute: RemoveSessionServer
```
### 2. File传输指定p2p链路

   1. A端执行 9 - CreateSessionServer，创建会话服务。
   2. B端执行 9 - CreateSessionServer，创建会话服务。
   3. A端(或B端)选择为文件发送端，执行 13 - SetFileSendListener, 注册文件发送监听。
   4. B端(或A端)选择为文件接收端，执行 14 - SetFileRecvListener, 注册文件接收监听。
   5. B端(或A端)执行 6 - GetLocalDeviceInfo，查看本端NetworkId值。
   6. A端（或B端）执行 11 - OpenSession，打开会话。
   7. A端（或B端）执行 18 - SendFile，发送文件。
   8. A端（或B端）执行 12 - CloseSession，关闭会话。
   9. A端执行 10 - RemoveSessionServer，删除会话服务。
   10. B端执行 10 - RemoveSessionServer，删除会话服务。
   
> *演示：*

    1. A端执行 9 - CreateSessionServer，创建会话服务
```text
******Softbus Tool Command List******
*               ...                 *
*     09 - CreateSessionServer      *
*               ...                 *
*************************************
==>Please input cmd index:9    /* 输入9，回车 */
    
Execute: CreateSessionServer
CreateSessionServer ok.
```
    2. B端执行 9 - CreateSessionServer，创建会话服务
```text
******Softbus Tool Command List******
*               ...                 *
*     09 - CreateSessionServer      *
*               ...                 *
*************************************
==>Please input cmd index:9    /* 输入9，回车 */
    
Execute: CreateSessionServer
CreateSessionServer ok.
```
    3. B端(或A端)执行 6 - GetLocalDeviceInfo，查看本端NetworkId值
```text
******Softbus Tool Command List******
*               ...                 *
*     06 - GetLocalDeviceInfo       *
*               ...                 *
*************************************
==>Please input cmd index:6    /* 输入6，回车 */

Execute: GetLocalDeviceInfo
>>>DeviceName: OpenHarmony 3.1 Release
>>>DeviceNetworkId: db57c55de79ac82d425b6f5db403f4667543fe540faa5fd71e30a167989f2b89
>>>Udid: C4F856BE5939A274C512F1CD5CDDDF7B49CD35A0E62C26330ABA386ABB127448
>>>Uuid: 869864402cf66944895fcaee0d64ee573676b4cdbe2bef4d6bed8c3ea5f3b23b
>>>ShortDevId: 1e7468f7
```
    4. A端(或B端)选择为文件发送端，执行 13 - SetFileSendListener, 注册文件发送监听
```text
******Softbus Tool Command List******
*               ...                 *
*     13 - SetFileSendListener      *
*               ...                 *
*************************************
==>Please input cmd index:13    /* 输入13，回车 */

Execute: SetFileSendListener
```
    5. B端(或A端)选择为文件接收端，执行 14 - SetFileRecvListener, 注册文件接收监听
```text
******Softbus Tool Command List******
*               ...                 *
*     14 - SetFileRecvListener      *
*               ...                 *
*************************************
==>Please input cmd index:14    /* 输入14，回车 */

Execute: SetFileRecvListener
==>Please input receive file path:/data/files    /* 输入接收文件的目录，回车 */
```
    6. A端（或B端）执行 11 - OpenSession，打开会话
```text
******Softbus Tool Command List******
*               ...                 *
*     11 - OpenSession              *
*               ...                 *
*************************************
==>Please input cmd index:11    /* 输入11，回车 */

Execute: OpenSession
==>Please input session type(1 - TYPE_MESSAGE, 2 - TYPE_BYTES, 3 - TYPE_FILE, 
4 - TYPE_STREAM):3    /* 输入3选择File，回车 */
==>Please input linkType num(0 - AUTO or [1, 4]):1    /* 输入数值来指定支持几种链路方式，
输入0为默认，输入1~4为指定1~4种链路方式，回车 */
Please input linkType[0]==>(1 - WLAN 5G, 2 - WLAN 2G, 3 - WIFI P2P, 4 - BR):3    /* 输入3选择
P2P链路方式，回车 */
==>Please input network Id:db57c55de79ac82d425b6f5db403f4667543fe540faa5fd71e30a167989
f2b89    /* 输入上一步查看的B端的NetworkId值，回车 */

4 nStackXDFile: NSTACKX_DFileClientWithTargetDev:[1313] :begin to Create Dfile client
...
4 nStackXDev: GetConnectionTypeByDev:[106] :ndevice name: p2p-wlan0-0
...
>>>OnSessionOpened sessionId = 1, result = 0.    /* OnSessionOpened回调信息 */
OpenSession succ, sessionId = 1    /* 会话打开成功 */

B端确认：
4 nStackXDFile: NSTACKX_DFileServer:[1148] :Begin to create dfile server
...
4 nStackXDFile: DFileReceiverHandle:[1788] :recv thread start

>>>OnSessionOpened sessionId = 1, result = 0.    /* OnSessionOpened回调信息 */
...
```
    7. A端（或B端）执行 18 - SendFile，发送文件
```text
******Softbus Tool Command List******
*               ...                 *
*     18 - SendFile                 *
*               ...                 *
*************************************
==>Please input cmd index:18    /* 输入18，回车 */

Execute: SendFile
==>Please input session Id:1    /* 输入打开的会话id，回车 */
==>Please input file path to SendFile:/data/files/testfile.txt    /* 输入发送的文件路径，回车 */

3 nStackXDFile: CheckSendFilesPara:[543] :send file with no user data.
SendFile ok
...
4 nStackXDFile: SetSendState:[637] :transId: 1, Send state: send file done -> send file done

>>>OnSendFileProcess sessionId = 1, bytesUpload = 5, total = 5    /* OnSendFileProcess回调信息 */

>>>OnSendFileFinished sessionId = 1, first file = testfile.txt    /* OnSendFileFinished回调信息 */
...

B端确认：
4 nStackXDFile: CalculateSessionTransferRatePrepare:[130] :begin to calulate transfer rate
...
>>>OnReceiveFileStarted sessionId = 1, first file = testfile.txt, fileCnt = 1    /* OnReceiveFileStarted回调信息 */
...
>>>OnReceiveFileFinished sessionId = 1, first file = testfile.txt, fileCnt = 1    /* OnReceiveFileFinished回调信息 */
...
2 nStackXDFile: DFileSessionHandleFrame:[864] :trans 1 is NULL && type is 7
```
    8. A端（或B端）执行 12 - CloseSession，关闭会话
```text
...    /* 前面步骤的回调信息，已经确认过 */
12     /* 输入12，回车 */

Execute: CloseSession
==>Please input session Id to CloseSession:1    /* 输入要关闭的会话的id，回车 */
...
4 nStackXDFile: NSTACKX_DFileClose:[1483] :finish to close session

B端确认：
4 nStackXDFile: NSTACKX_DFileClose:[1449] :begin to close session
...
>>>OnSessionClosed sessionId = 1.    /* OnSessionClosed回调信息 */
```
    9. A端执行 10 - RemoveSessionServer，删除会话服务
```text
******Softbus Tool Command List******
*               ...                 *
*     10 - RemoveSessionServer      *
*               ...                 *
*************************************
==>Please input cmd index:10    /* 输入10，回车 */

Execute: RemoveSessionServer
```
    10. B端执行 10 - RemoveSessionServer，删除会话服务
```text
******Softbus Tool Command List******
*               ...                 *
*     10 - RemoveSessionServer      *
*               ...                 *
*************************************
==>Please input cmd index:10     /* 输入10，回车 */

Execute: RemoveSessionServer
```
### 3. Message传输指定p2p链路

   1. A端执行 9 - CreateSessionServer，创建会话服务。
   2. B端执行 9 - CreateSessionServer，创建会话服务。
   3. B端执行 6 - GetLocalDeviceInfo，查看本端NetworkId值。
   4. A端（或B端）执行 11 - OpenSession，打开会话。
   5. A端（或B端）执行 15 - SendMessage，发送Message。
   6. A端（或B端）执行 12 - CloseSession，关闭会话。
   7. A端执行 10 - RemoveSessionServer，删除会话服务。
   8. B端执行 10 - RemoveSessionServer，删除会话服务。

> *演示：*

```text
参照1. Bytes传输指定p2p链路
```

### 4. Stream传输指定p2p链路

   1. A端执行 9 - CreateSessionServer，创建会话服务。
   2. B端执行 9 - CreateSessionServer，创建会话服务。
   3. B端执行 6 - GetLocalDeviceInfo，查看本端NetworkId值。
   4. A端（或B端）执行 11 - OpenSession，打开会话。
   5. A端（或B端）执行 17 - SendStream，发送Stream。
   6. A端（或B端）执行 12 - CloseSession，关闭会话。
   7. A端执行 10 - RemoveSessionServer，删除会话服务。
   8. B端执行 10 - RemoveSessionServer，删除会话服务。

> *演示：*

```text
参照1. Bytes传输指定p2p链路
```
